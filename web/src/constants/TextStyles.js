import { css } from "styled-components"

export const HeadlineBold = css`
  font-size: 64px;
  font-family: "Prompt", sans-serif;
  line-height: 21px;
`
export const BodyRegular = css`
  font-size: 16px;
  font-family: "Prompt", sans-serif;
  line-height: 14px;
`
export const CaptionRegular = css`
  font-size: 12px;
  font-family: "Prompt", sans-serif;
  line-height: 12px;
`
