import { colors } from "../../constants/Colors"
import { HeadlineBold } from "../../constants/TextStyles"
import styled from "styled-components"

export const Container = styled.div`
  padding-top: 50px;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;

  padding-bottom: 50px;
`

export const Header = styled.h1`
  ${HeadlineBold}
  color:${colors.SKY_BLUE};
  font-weight: 300;
`
