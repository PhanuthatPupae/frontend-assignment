export const colors = {
  SKY_BLUE: "#2c9cdb",
  WHITE: "#FFFFFF",
  LIGHT_GREY: "#BBC1C7",
  DUST: "#E5E5E5",
  BLACK: "#000000",
}
