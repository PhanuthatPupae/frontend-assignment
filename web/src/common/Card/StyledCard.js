import { colors } from "../../constants/Colors"
import styled from "styled-components"
import Truncate from "react-truncate"
import { BodyRegular, CaptionRegular } from "../../constants/TextStyles"

export const CardContainer = styled.div`
  width: 800px;
  max-height: 320px;
  display: flex;
  flex-direction: row;
  padding: 20px;

  .column-left {
    flex-direction: column;
    width: 40%;
    float: left;
    img {
      width: 100%;
      height: 320px;
      border-radius: 10px;
    }
  }
  .column-right {
    display: flex;
    justify-content: space-between;
    flex-direction: column;
    width: 60%;
    padding: 0 20px 0 20px;
    . {
    }
  }
`
export const Description = styled(Truncate)`
  ${BodyRegular}
  color:${colors.LIGHT_GREY};
  white-space: pre-line;
  font-weight: 300;
  text-align: justify;
  .link-read-more {
    color:${colors.SKY_BLUE};
  }

`
export const GridContainer = styled.div`
  display: grid;
  grid-column-gap: 20px;
  grid-template-columns: auto auto auto;
  width: 100%;
`
export const Title = styled.h2`
  ${BodyRegular}
  font-weight: bold;
  cursor: pointer;
`
export const Tag = styled.div`
  ${CaptionRegular};
  color: ${colors.LIGHT_GREY};
  flex-direction: row;
  .tag-underline {
    text-decoration: underline;
    margin-left: 5px;
    cursor: pointer;
  }
`
export const ImgPhoto = styled.img`
  width: 100%;
  height: 100px;
  border-radius: 10px;
`
