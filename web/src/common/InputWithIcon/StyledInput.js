import styled from "styled-components"
import { BodyRegular } from "../../constants/TextStyles"
import { colors } from "../../constants/Colors"

export const Input = styled.input`
  border-width: 0 0 1px 0;
  width: 100%;
  text-align: center;
  padding-right: 40px;
  ${BodyRegular};
  &:focus {
    outline: none;
    border-bottom-color: ${colors.SKY_BLUE};
  }
  ::placeholder {
    text-align: center;
  }
`
export const Contaniner = styled.div`
  align-items: center;
  position: relative;
  display: flex;
  flex-direction: row;
  margin-top: 20px;
  margin-bottom: 20px;
  width: ${(props) => (props.width ? props.width + "px" : "800px")};
`
export const Image = styled.img`
  width: 15px;
  height: 15px;
  position: absolute;
  ${(props) => (props.iconRight ? "right: 10px;" : "left:10px;")}
  cursor: pointer;
`
