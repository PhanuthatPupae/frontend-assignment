import React from 'react'
export const newLine = (longText) => {
  return longText.split("\n").map((text, i, arr) => {
    const line = <span key={i}>{text}</span>
    if (i === arr.length - 1) {
      return line
    } else {
      return [line, <br key={i + "br"} />]
    }
  })
}
