import React, { Component } from "react"
import {
  CardContainer,
  Title,
  Description,
  Tag,
  GridContainer,
  ImgPhoto,
} from "./StyledCard"
import PropTypes from "prop-types"
import { newLine } from "../../util"
const ReadMore = ({ url }) => (
  <span>
    ...{" "}
    <a className='link-read-more' href={url}>
      อ่านต่อ
    </a>
  </span>
)

export default class Card extends Component {
  render() {
    const { data, numberLineDescription, onClickTag, onClickTitle } = this.props

    return (
      <CardContainer key={data.eid}>
        <div className='column-left'>
          <img src={data.photos[0]} alt={data.eid+"travel-photo"}/>
        </div>
        <div className='column-right'>
          <div>
            <Title onClick={() => onClickTitle(data.url)}>{data.title}</Title>
            <Description
              lines={numberLineDescription}
              ellipsis={<ReadMore url={data.url} />}
            >
              {newLine(data.description)}
            </Description>
            <Tag>
              <p>
                หมวด{" "}
                {data?.tags.map((tag) => (
                  <span
                    className='tag-underline'
                    onClick={() => onClickTag(tag)}
                  >
                    {tag}
                  </span>
                ))}
              </p>
            </Tag>
          </div>
          <GridContainer>
            {data?.photos.slice(1).map((photos,i) => (
              <ImgPhoto src={photos} className='image-photo' alt={`${i}-photo`}/>
            ))}
          </GridContainer>
        </div>
      </CardContainer>
    )
  }
}
Card.propTypes = {
  data: PropTypes.object.isRequired,
  onClickTag: PropTypes.func,
  onClickTitle: PropTypes.func,
  numberLineDescription: PropTypes.number,
}

Card.defaultProps = {
  data: [],
  onClickTag: () => {},
  onClickTitle: () => {},
  numberLineDescription: 2,
}
