import React, { Component } from "react"
import PropTypes from "prop-types"
import Card from "../Card/Card"
export default class CardList extends Component {
  render() {
    const {
      listData,
      onClickTag,
      onClickTitle,
      numberLineDescription,
    } = this.props
    return (
      <div data-testid='card-list'>
        {listData.map((data) => (
          <Card
            key={data.eid}
            data={data}
            onClickTag={onClickTag}
            onClickTitle={onClickTitle}
            numberLineDescription={numberLineDescription}
          />
        ))}
      </div>
    )
  }
}
CardList.propTypes = {
  listData: PropTypes.array.isRequired,
  onClickTag: PropTypes.func,
  onClickTitle: PropTypes.func,
  numberLineDescription: PropTypes.number,
}

CardList.defaultProps = {
  listData: [],
  onClickTag: () => {},
  onClickTitle: () => {},
  numberLineDescription: 2,
}
