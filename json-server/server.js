const jsonServer = require("json-server")
const server = jsonServer.create()
const router = jsonServer.router("db.json")
const middlewares = jsonServer.defaults()

server.use(middlewares)

server.use(
  jsonServer.rewriter({
    "/api/trips?keywords?=:searchstring": "/trips?q=:searchstring",
    "/api/*": "/$1",
  }),
)

server.use(router)

server.listen(9000, () => {
  console.log("JSON Server is running PORT 9000")
})
